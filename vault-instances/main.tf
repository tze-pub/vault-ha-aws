data "aws_ami" "default" {
  most_recent = "true"

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Ubuntu AWS AMI ID
}

locals {
  # Hard code AMI ID to avoid updating instance with newer AMI
  ami_id = data.aws_ami.default.image_id

  # Server hostnames
  hostname01 = "vault01.aiyor.net"
  hostname02 = "vault02.aiyor.net"

  # Key Pair
  key_pair = "tl-ssh"

  # User data template file path
  user_data_tpl = "./cloud-init.yml.tmpl"

  # Subnet maps
  subnet = "subnet-02b6dbefad2b5c548"

  iam_instance_profile = "vault-profile"
}


module "vault01" {
  source = "git@gitlab.com:tze-group/terraform/modules/aws-ec2.git"

  host_name                  = local.hostname01
  instance_type              = "t2.micro"
  iam_instance_profile       = local.iam_instance_profile
  ami                        = local.ami_id
  key_name                   = local.key_pair
  subnet_id                  = local.subnet
  vpc_security_group_ids     = ["sg-01917e08818ae179c"]
  assign_public_ip           = true
  root_volume_size           = 8
  root_delete_on_termination = true
  root_volume_type           = "gp2"
  # lb_target_groups = [
  #   aws_lb_target_group.int_vault_tg.arn,
  #   aws_lb_target_group.ext_vault_tg.arn,
  # ]

  user_data_base64 = base64encode(templatefile(local.user_data_tpl, {
    fqdn      = local.hostname01,
    host_name = split(".", local.hostname01)[0]
  }))

  tags = {
    "AnsibleBuild" = "Vault"
    "VaultCluster" = "UniqueCluster01"
  }
}

module "vault02" {
  source = "git@gitlab.com:tze-group/terraform/modules/aws-ec2.git"

  host_name                  = local.hostname02
  instance_type              = "t2.micro"
  iam_instance_profile       = local.iam_instance_profile
  ami                        = local.ami_id
  key_name                   = local.key_pair
  subnet_id                  = local.subnet
  vpc_security_group_ids     = ["sg-01917e08818ae179c"]
  assign_public_ip           = true
  root_volume_size           = 8
  root_delete_on_termination = true
  root_volume_type           = "gp2"
  lb_target_groups = [
    aws_lb_target_group.int_vault_tg.arn,
    aws_lb_target_group.ext_vault_tg.arn,
  ]


  user_data_base64 = base64encode(templatefile(local.user_data_tpl, {
    fqdn      = local.hostname02,
    host_name = split(".", local.hostname02)[0]
  }))

  tags = {
    "AnsibleBuild" = "Vault"
    "VaultCluster" = "UniqueCluster01"
  }
}

/*
module "test" {
  #source = "git::git@gitlab.com:orica/terraform-modules/aws-ec2.git?ref=v0.1.1"
  source = "../../../terraform/modules/aws-ec2"

  host_name                  = "test.aiyor.net"
  instance_type              = "t2.micro"
  iam_instance_profile       = local.iam_instance_profile
  ami                        = local.ami_id
  key_name                   = local.key_pair
  subnet_id                  = local.subnet #"subnet-0362ed7b005b74ae1"
  vpc_security_group_ids     = ["sg-01917e08818ae179c"]
  assign_public_ip           = true
  root_volume_size           = 8
  root_delete_on_termination = true
  root_volume_type           = "gp2"

  user_data_base64 = base64encode(templatefile(local.user_data_tpl, {
    fqdn      = "test.aiyor.net",
    host_name = split(".", "test.aiyor.net")[0]
  }))

  tags = {
    "Function" = "Test"
  }
}
*/