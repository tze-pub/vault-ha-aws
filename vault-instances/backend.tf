terraform {
  backend "s3" {
    bucket = "tl-shopping-tf-bucket"
    key    = "sandpit/vault-deployment/vault-instances"
    region = "ap-southeast-2"
  }
}

