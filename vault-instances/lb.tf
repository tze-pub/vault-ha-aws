resource "aws_lb" "ext_lb" {
  name               = "ext-vault-lb"
  internal           = false
  load_balancer_type = "network"
  subnets            = [local.subnet]

  enable_deletion_protection = false

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group" "ext_vault_tg" {
  name        = "ext-vault-tg"
  port        = 8200
  protocol    = "TCP"
  target_type = "instance"
  vpc_id      = data.aws_subnet.this.vpc_id

  stickiness {
    type    = "lb_cookie"
    enabled = false
  }

  health_check {
    enabled  = true
    path     = "/v1/sys/health"
    protocol = "HTTP"
    port     = "8200"
  }
}


resource "aws_lb_listener" "ext_listener" {
  load_balancer_arn = aws_lb.ext_lb.arn
  port              = "8200"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ext_vault_tg.arn
  }
}




resource "aws_lb" "int_lb" {
  name               = "int-vault-lb"
  internal           = true
  load_balancer_type = "network"
  subnets            = [local.subnet]

  enable_deletion_protection = false

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group" "int_vault_tg" {
  name        = "int-vault-tg"
  port        = 8200
  protocol    = "TCP"
  target_type = "instance"
  vpc_id      = data.aws_subnet.this.vpc_id

  stickiness {
    type    = "lb_cookie"
    enabled = false
  }

  health_check {
    enabled  = true
    path     = "/v1/sys/health"
    protocol = "HTTP"
    port     = "8200"
  }
}


resource "aws_lb_listener" "int_listener" {
  load_balancer_arn = aws_lb.int_lb.arn
  port              = "8200"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.int_vault_tg.arn
  }
}
