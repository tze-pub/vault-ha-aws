resource "aws_kms_key" "this" {
  description             = "Vault KMS Key"
  key_usage               = "ENCRYPT_DECRYPT"
  deletion_window_in_days = 7
}

resource "aws_kms_alias" "this" {
  name          = "alias/hashicorp-vault"
  target_key_id = aws_kms_key.this.key_id
}
