variable "dynamodb_table_name" {
  type    = string
  default = "vault-dyndb-table"
}
