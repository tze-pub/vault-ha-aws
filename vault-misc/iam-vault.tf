resource "aws_iam_group" "vault" {
  name = "iam-grp-vault"
  path = "/users/"
}

resource "aws_iam_policy" "vault_policy" {
  name        = "vault-policy"
  description = "Hashicorp Vault IAM Policy"

  policy = templatefile("iam-policies/vault-iam-policy.json",
    {
      vault_iam_grp_arn = aws_iam_group.vault.arn
    }
  )
}

resource "aws_iam_group_policy_attachment" "vault_attach" {
  group      = aws_iam_group.vault.name
  policy_arn = aws_iam_policy.vault_policy.arn
}

resource "aws_iam_user" "vault" {
  name = "vault"

  tags = {
    Name        = "vault"
    Description = "Hashicorp Vault IAM User"
  }
}

resource "aws_iam_user_group_membership" "vault" {
  user = aws_iam_user.vault.name

  groups = [
    aws_iam_group.vault.name,
  ]
}


resource "aws_iam_policy" "assume_role_policy" {
  name        = "sts-assumerole-policy"
  description = "General STS AssumeRole Policy"

  policy = file("iam-policies/assume-role-policy.json")
}

data "aws_caller_identity" "this" {}

resource "aws_iam_role" "assumer_role" {
  name = "assumer-role"
  path = "/"

  assume_role_policy = templatefile("iam-policies/assume-role-trust-relationship.json",
    {
      account_id = data.aws_caller_identity.this.account_id
    }
  )
}

resource "aws_iam_role_policy_attachment" "assumer_role_attach" {
  role       = aws_iam_role.assumer_role.name
  policy_arn = aws_iam_policy.assume_role_policy.arn
}


resource "aws_iam_role" "assumee_admin_role" {
  name = "assumee-admin-role"
  path = "/"

  assume_role_policy = templatefile("iam-policies/assume-role-trustee.json",
    {
      trusted_arn = aws_iam_role.assumer_role.arn
    }
  )
}

resource "aws_iam_role_policy_attachment" "assumee_admin_role_attach" {
  role       = aws_iam_role.assumee_admin_role.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}


