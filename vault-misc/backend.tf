terraform {
  backend "s3" {
    bucket = "tl-shopping-tf-bucket"
    key    = "sandpit/vault-deployment/vault-misc"
    region = "ap-southeast-2"
  }
}

