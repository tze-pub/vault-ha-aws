resource "aws_iam_instance_profile" "this" {
  name = "vault-profile"
  role = aws_iam_role.instance_role.name
}

resource "aws_iam_role" "instance_role" {
  name = "vault-instance-role"
  path = "/"

  assume_role_policy = file("iam-policies/instance-assume-role-policy.json")
}

resource "aws_iam_policy" "instance_role_policy" {
  name        = "vault-instance-role-policy"
  description = "Hashicorp Vault Instance Role Policy"

  policy = templatefile("iam-policies/vault-instance-role-policy.json",
    {
      dynamodb_arn = aws_dynamodb_table.this.arn,
      kms_key_arn  = aws_kms_key.this.arn
    }
  )
}

resource "aws_iam_role_policy_attachment" "this" {
  role       = aws_iam_role.instance_role.name
  policy_arn = aws_iam_policy.instance_role_policy.arn
}
