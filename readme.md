# Terraform and Ansible Deployment for Hashicorp Vault (HA) in AWS

This repository contains Terraform and Ansible codes for deploying Hashicorp Vault (HA) in AWS.

## Vault Setup

* Storage Backend: AWS DynamoDB
* Auto-unsesal: AWS KMS
* Compute: 2x EC2 instances
* Miscallaneous: IAM policies, roles and instance profile for passwordless access to KMS an DynamoDB

### Information

The design of this deployment does not require any hardcoded access credential to be stored in text or environment variables for the EC2 instances to access DynamoDB and KMS.  An IAM instance profile will be attached to the EC2 instances, and Hashicorp Vault will access DynamoDB and KMS from the IAM instance profile policy.

### Note

This deployment configures EIP to the instances in order for Ansible to reach in to configure the machines.  This is not required if there is a private Gitlab runner that can connect to the EC2 instance using their private IP.  

This can be configured/modified at the Ansible playbook level to use private IP (if there is a private runner)

## Design Diagram

The following diagram shows the outcome of the deployment using the IaC in this repository.

![alt text](std-ha-vault.svg "Vault HA Design in AWS")



Authored by: Tze Liang
